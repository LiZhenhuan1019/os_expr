#include <iostream>
#include <vector>
#include <thread>
#include <random>
#include <pthread.h>
#include <sys/types.h>
#include <sys/sem.h>

void P(int semid, int index)
{
    sembuf sem;
    sem.sem_num = index;
    sem.sem_op = -1;
    sem.sem_flg = 0;
    semop(semid, &sem, 1);
}
void V(int semid, int index)
{
    sembuf sem;
    sem.sem_num = index;
    sem.sem_op = 1;
    sem.sem_flg = 0;
    semop(semid, &sem, 1);
}
union semun
{
    int val;
    struct semid_ds *buf;
    unsigned short *array;
};
const int ticket_amount = 500;
std::size_t remaining_tickets = ticket_amount;
struct shared_data
{
    std::size_t thread_amount;
    int mutex;
    int sem_a, sem_b;
};
struct seller_arg
{
    shared_data &shared;
    std::size_t thread_index;
    std::size_t number_of_ticket_request;
};
void *seller(void *arg)
{
    auto *a = static_cast<seller_arg *>(arg);
    while (true)
    {
        P(a->shared.sem_b, a->thread_index);
        std::size_t number_of_tickets = a->number_of_ticket_request;
        V(a->shared.sem_a, a->thread_index);
        P(a->shared.mutex, 0);
        if (remaining_tickets == 0)
        {
            V(a->shared.mutex, 0);
            return nullptr;
        }
        if (remaining_tickets < number_of_tickets)
            number_of_tickets = remaining_tickets;
        remaining_tickets -= number_of_tickets;
        std::cout << "Thread " << a->thread_index << " sold " << number_of_tickets << " tickets, remaining " << remaining_tickets << "\n";
        V(a->shared.mutex, 0);
    }
}
int main()
{
    const std::size_t thread_amount = std::thread::hardware_concurrency();
    shared_data data{};
    data.thread_amount = thread_amount;
    data.mutex = semget(1, 1, IPC_CREAT | 0666);
    data.sem_a = semget(2, thread_amount, IPC_CREAT | 0666);
    data.sem_b = semget(3, thread_amount, IPC_CREAT | 0666);
    std::vector<unsigned short> init_a(thread_amount, 1);
    std::vector<unsigned short> init_b(thread_amount, 0);
    std::vector<pthread_t> thread_ids(thread_amount);
    semctl(data.mutex, 0, SETVAL, semun{.val=1});
    semctl(data.sem_a, 0, SETALL, semun{.array=&init_a[0]});
    semctl(data.sem_b, 0, SETALL, semun{.array=&init_b[0]});
    std::vector<seller_arg> args(thread_amount, seller_arg{data});
    for (std::size_t i = 0; i < thread_amount; ++i)
    {
        args[i].thread_index = i;
        if (pthread_create(&thread_ids[i], nullptr, seller, &args[i]) != 0)
        {
            std::cout << "Create thread failed.\n";
            return -1;
        }
    }
    std::random_device r;
    std::default_random_engine el(r());
    int i = 0;
    P(data.mutex, 0);
    while (remaining_tickets != 0)
    {
        V(data.mutex, 0);
        P(data.sem_a, i);
        args[i].number_of_ticket_request = std::uniform_int_distribution<std::size_t>(1, 6)(el);
        V(data.sem_b, i);
        ++i;
        if (i == thread_amount)
            i = 0;
        P(data.mutex, 0);
    }
    V(data.mutex, 0);
    for (std::size_t i = 0; i < thread_amount; ++i)
    {
        V(data.sem_b, i);
        void *result;
        pthread_join(thread_ids[i], &result);
    }
    semctl(data.sem_b, 0, IPC_RMID);
    semctl(data.sem_a, 0, IPC_RMID);
    semctl(data.mutex, 0, IPC_RMID);
}