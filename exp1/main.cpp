#include <iostream>
#include <unistd.h>
#include <sys/wait.h>
#include <signal.h>
pid_t sub_1, sub_2;
int sub_1_fd, sub_2_fd;
void interrupt_handler(int)
{
    kill(sub_1, SIGUSR1);
    kill(sub_2, SIGUSR2);
}
void siguser1_handler(int)
{
    std::cout << "Child Process 1 is Killed by Parent!\n";
    close(sub_1_fd);
    std::exit(0);
}
void siguser2_handler(int)
{
    std::cout << "Child Process 2 is Killed by Parent!\n";
    close(sub_2_fd);
    std::exit(0);
}

void exec_sub_1(int istream, int ostream)
{
    sub_1_fd = ostream;
    close(istream);
    signal(SIGINT, SIG_IGN);
    signal(SIGUSR1, siguser1_handler);
    std::string s;
    for (std::size_t i = 1; ; ++i)
    {
        s = "I send you ";
        s += std::to_string(i);
        s += " times.\n";
        write(ostream, s.data(), s.size());
        sleep(1);
    }
}
void exec_sub_2(int istream, int ostream)
{
    sub_2_fd = istream;
    close(ostream);
    signal(SIGINT, SIG_IGN);
    signal(SIGUSR2, siguser2_handler);
    std::string s(10, '\0');
    while (1)
    {
        long size = read(istream, s.data(), s.size());
        std::cout << s;
    }
}
int main()
{
    int pipefd[2]{};
    if (int status = pipe(pipefd))
        return status;
    if (signal(SIGINT, interrupt_handler) == SIG_ERR)
        return -1;
    switch (int id = fork())
    {
        case -1:
            return -1;
        case 0:
            exec_sub_1(pipefd[0], pipefd[1]);
            break;
        default:
            sub_1 = id;
    }
    switch (int id = fork())
    {
        case -1:
            return -1;
        case 0:
            exec_sub_2(pipefd[0], pipefd[1]);
            break;
        default:
            sub_2 = id;
    }
    close(pipefd[0]);
    close(pipefd[1]);
    waitpid(sub_1, nullptr, 0);
    waitpid(sub_2, nullptr, 0);
    std::cout << "Parent Process is Killed!\n";
    return 0;
}