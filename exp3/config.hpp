#ifndef OS_EXPR_CONFIG_HPP
#define OS_EXPR_CONFIG_HPP

constexpr int buffer_length = 10240;
constexpr int mem_number = 10;
constexpr const char in_file_name[] = "[Kamigami] Hyouka - 13 (BDrip 1920x1080 x264 Hi10P FLAC).mkv";
constexpr const char out_file_name[] = "out.mkv";
#endif //OS_EXPR_CONFIG_HPP
