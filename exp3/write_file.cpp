#include <semaphore.h>
#include <fstream>
#include "config.hpp"
#include "shared_memory.hpp"
#include "semaphore.hpp"
#include "make_array.hpp"
int main()
{
    std::array<key_t, mem_number> keys;
    for (int i = 0; i < mem_number; ++i)
    {
        keys[i] = ftok("identity", i);
        printf("keys[%d] = %d\n", i, keys[i]);
    }
    std::array<key_t, mem_number> count_keys;
    for (int i = 0; i < mem_number; ++i)
    {
        count_keys[i] = ftok("identity", i + mem_number);
        printf("count_keys[%d] = %d\n", i, count_keys[i]);
    }
    auto memories = make_array<shared_memory_view<buffer_length>>(keys);
    auto sems = make_array<semaphore_view<2>>(keys);
    auto count_shm = make_array<shared_memory_view<sizeof(long)>>(count_keys);
    std::ofstream out(out_file_name, std::ios_base::binary);
    bool exit = false;
    printf("writing file\n");
    for (int i = 0; out.good() && !exit; i = (i + 1) % mem_number)
    {
        sems[i].lock(1);
        long count = (long &) (*count_shm[i].pointer);
        out.write(memories[i].pointer, count);
        printf("read %ld bytes from shared memory.\n", (long &) (*count_shm[i].pointer));
        if (count != buffer_length)
            exit = true;
        sems[i].unlock(0);
    }
}