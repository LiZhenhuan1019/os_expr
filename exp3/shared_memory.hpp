#ifndef OS_EXPR_SHARED_MEMORY_HPP
#define OS_EXPR_SHARED_MEMORY_HPP
#include <cstddef>
#include <array>
#include <sys/types.h>
#include <sys/shm.h>
#include <stdexcept>
template <std::size_t buffer_length>
struct shared_memory_view
{
    explicit shared_memory_view(key_t key)
        : shmid(shmget(key, buffer_length, 0666 | IPC_CREAT)), pointer((char *) shmat(shmid, nullptr, 0))
    {
        if (shmid < 0)
            throw std::runtime_error("create shared memory failed.");
    }
    shared_memory_view(shared_memory_view &&src) noexcept
        : shmid(src.shmid), pointer(std::exchange(src.pointer, nullptr))
    {
    }
    shared_memory_view &operator=(shared_memory_view &&rhs) noexcept
    {
        std::swap(shmid, rhs.shmid);
        std::swap(pointer, rhs.pointer);
        return *this;
    }
    ~shared_memory_view()
    {
        shmdt(pointer);
    }
    int shmid;
    char *pointer;
};
template <std::size_t buffer_length>
struct shared_memory
{
    explicit shared_memory(key_t key)
        : shmid(shmget(key, buffer_length, 0666 | IPC_CREAT))
    {
        if (shmid < 0)
            throw std::runtime_error("create shared memory failed.");
    }
    shared_memory(shared_memory &&src) noexcept
        : shmid(std::exchange(src.semid, -1))
    {}
    shared_memory &operator=(shared_memory &&rhs) noexcept
    {
        std::swap(shmid, rhs.semid);
        return *this;
    }
    ~shared_memory()
    {
        shmctl(shmid, IPC_RMID, nullptr);
    }
    int shmid;
};

#endif //OS_EXPR_SHARED_MEMORY_HPP
