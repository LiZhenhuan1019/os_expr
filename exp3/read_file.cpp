#include <semaphore.h>
#include <fstream>
#include "config.hpp"
#include "shared_memory.hpp"
#include "semaphore.hpp"
#include "make_array.hpp"

int main()
{
    std::array<key_t, mem_number> keys;
    for (int i = 0; i < mem_number; ++i)
    {
        keys[i] = ftok("identity", i);
        printf("keys[%d] = %d\n", i, keys[i]);
    }
    std::array<key_t, mem_number> count_keys;
    for (int i = 0; i < mem_number; ++i)
    {
        count_keys[i] = ftok("identity", i + mem_number);
        printf("count_keys[%d] = %d\n", i, count_keys[i]);
    }
    auto memories = make_array<shared_memory_view<buffer_length>>(keys);
    auto sems = make_array<semaphore_view<2>>(keys);
    auto count_shm = make_array<shared_memory_view<sizeof(long)>>(count_keys);
    std::ifstream in(in_file_name, std::ios_base::binary);
    printf("reading file\n");
    for (int i = 0; in.good(); i = (i + 1) % mem_number)
    {
        sems[i].lock(0);
        in.read(memories[i].pointer, buffer_length);
        (long &) (*count_shm[i].pointer) = in.gcount();
        printf("write %ld bytes into shared memory.\n", (long &) (*count_shm[i].pointer));
        sems[i].unlock(1);
    }
}