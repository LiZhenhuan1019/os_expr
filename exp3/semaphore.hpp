#ifndef OS_EXPR_SEMAPHORE_HPP
#define OS_EXPR_SEMAPHORE_HPP
#include <cstddef>
#include <array>
#include <sys/types.h>
#include <sys/sem.h>
template <std::size_t num_sem>
struct semaphore_view
{
    explicit semaphore_view(key_t key)
        : semid(semget(key, (int) num_sem, 0666 | IPC_CREAT))
    {
        if (semid < 0)
            throw std::runtime_error("create semaphore failed.");
    }
    void lock(std::size_t index)
    {
        sembuf sem;
        sem.sem_num = (unsigned short) index;
        sem.sem_op = -1;
        sem.sem_flg = 0;
        semop(semid, &sem, 1);
    }
    void unlock(std::size_t index)
    {
        sembuf sem;
        sem.sem_num = (unsigned short) index;
        sem.sem_op = 1;
        sem.sem_flg = 0;
        semop(semid, &sem, 1);
    }
    int semid;
};
template <std::size_t num_sem>
struct semaphore
{
    explicit semaphore(key_t key)
        : semid(semget(key, (int) num_sem, 0666 | IPC_CREAT))
    {
        if (semid < 0)
            throw std::runtime_error("create semaphore failed.");
    }
    semaphore(semaphore &&src) noexcept
        : semid(std::exchange(src.semid, -1))
    {}
    semaphore &operator=(semaphore &&rhs) noexcept
    {
        std::swap(semid, rhs.semid);
        return *this;
    }
    void lock(std::size_t index)
    {
        sembuf sem;
        sem.sem_num = (unsigned short) index;
        sem.sem_op = -1;
        sem.sem_flg = 0;
        semop(semid, &sem, 1);
    }
    void unlock(std::size_t index)
    {
        sembuf sem;
        sem.sem_num = (unsigned short) index;
        sem.sem_op = 1;
        sem.sem_flg = 0;
        semop(semid, &sem, 1);
    }
    ~semaphore()
    {
        semctl(semid, 0, IPC_RMID);
    }
    int semid;
};

#endif //OS_EXPR_SEMAPHORE_HPP
