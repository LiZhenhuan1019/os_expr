#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <sys/wait.h>
#include <unistd.h>
#include <cstdio>
#include <fstream>
#include "config.hpp"
#include "shared_memory.hpp"
#include "semaphore.hpp"
#include "make_array.hpp"
union semun
{
    int val;
    struct semid_ds *buf;
    unsigned short *array;
};
int main()
{
    std::array<key_t, mem_number> keys;
    for (int i = 0; i < mem_number; ++i)
    {
        keys[i] = ftok("identity", i);
        printf("keys[%d] = %d\n", i, keys[i]);
    }
    std::array<key_t, mem_number> count_keys;
    for (int i = 0; i < mem_number; ++i)
    {
        count_keys[i] = ftok("identity", i + mem_number);
        printf("count_keys[%d] = %d\n", i, count_keys[i]);
    }
    auto memories_owner = make_array<shared_memory<buffer_length>>(keys);
    auto sems_onwer = make_array<semaphore<2>>(keys);
    auto count_shm_onwer = make_array<shared_memory<sizeof(long)>>(count_keys);
    unsigned short init[] = {1, 0};
    for (auto const &each : sems_onwer)
    {
        semctl(each.semid, 0, SETALL, semun{.array = init});
    }
    pid_t read_file, write_file;
    if ((read_file = fork()) == 0)
    {
        char exe_name[] = "exp3_read_file";
        char *const arg[] = {exe_name, nullptr};
        execv(arg[0], arg);
        return -1;
    }
    if ((write_file = fork()) == 0)
    {
        char exe_name[] = "exp3_write_file";
        char *const arg[] = {exe_name, nullptr};
        execv(arg[0], arg);
        return -1;
    }
    std::printf("waiting for sub process.\n");
    waitpid(read_file, nullptr, 0);
    waitpid(write_file, nullptr, 0);
}