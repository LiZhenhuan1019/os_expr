#ifndef OS_EXPR_MAKE_ARRAY_HPP
#define OS_EXPR_MAKE_ARRAY_HPP

#include <cstddef>
#include <array>
namespace detail
{
    template <typename T, std::size_t ...Indices>
    std::array<T, sizeof...(Indices)> make_array_impl(std::array<key_t, sizeof...(Indices)> const &keys, std::index_sequence<Indices...>)
    {
        return {T(keys[Indices])...};
    }
}
template <typename T, std::size_t mem_number>
std::array<T, mem_number> make_array(std::array<key_t, mem_number> const &keys)
{
    return detail::make_array_impl<T>(keys, std::make_index_sequence<mem_number>{});
}
#endif //OS_EXPR_MAKE_ARRAY_HPP
