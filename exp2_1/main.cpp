#include <iostream>
#include <pthread.h>
#include <sys/types.h>
#include <sys/sem.h>

void P(int semid, int index)
{
    sembuf sem;
    sem.sem_num = index;
    sem.sem_op = -1;
    sem.sem_flg = 0;
    semop(semid, &sem, 1);
}
void V(int semid, int index)
{
    sembuf sem;
    sem.sem_num = index;
    sem.sem_op = 1;
    sem.sem_flg = 0;
    semop(semid, &sem, 1);
}
union semun
{
    int val;
    struct semid_ds *buf;
    unsigned short *array;
};
struct shared_data
{
    int a;
    int semid;
};
void *thread_1(void *arg)
{
    shared_data *data = static_cast<shared_data *>(arg);
    for (int i = 1; i <= 100; ++i)
    {
        P(data->semid, 0);
        data->a += i;
        V(data->semid, 1);
    }
    pthread_exit(nullptr);
}
void *thread_2(void *arg)
{
    shared_data *data = static_cast<shared_data *>(arg);
    for (int i = 0; i < 100; ++i)
    {
        P(data->semid, 1);
        std::cout << data->a << "\n";
        V(data->semid, 0);
    }
    pthread_exit(nullptr);
}
int main()
{
    shared_data data{};
    data.semid = semget(IPC_PRIVATE, 2, IPC_CREAT | 0666);
    unsigned short init[2] = {1, 0};
    semctl(data.semid, 0, SETALL, semun{.array=init});
    pthread_t t1, t2;
    if (pthread_create(&t1, nullptr, thread_1, &data) != 0)
    {
        std::cout << "Create thread 1 failed.\n";
        return -1;
    }
    if (pthread_create(&t2, nullptr, thread_2, &data) != 0)
    {
        std::cout << "Create thread 2 failed.\n";
        return -1;
    }
    void *result;
    pthread_join(t1, &result);
    pthread_join(t2, &result);
    semctl(data.semid, 0, IPC_RMID);
}