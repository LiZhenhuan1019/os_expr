#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>
#include <pwd.h>
#include <grp.h>
#include <time.h>
#include <locale.h>
#include <langinfo.h>
#include <cstdio>
#include <string>
#include <string_view>

void indented_print(char const *content, std::size_t depth)
{
    std::string spaces(depth, ' ');
    for (std::size_t i = 0; i < depth; ++i)
        spaces[i] = ' ';
    spaces[depth] = '\0';
    printf("%s%s", spaces.data(), content);
}

void print_mode(mode_t mode)
{
    char type;
    switch (mode & S_IFMT)
    {
        case S_IFBLK:
            type = 'b';
            break;
        case S_IFCHR:
            type = 'c';
            break;
        case S_IFDIR:
            type = 'd';
            break;
        case S_IFIFO:
            type = 'p';
            break;
        case S_IFLNK:
            type = 'l';
            break;
        case S_IFREG:
            type = '-';
            break;
        case S_IFSOCK:
            type = 's';
            break;
        default:
            type = '?';
    }
    putchar(type);
    printf((mode & S_IRUSR) ? "r" : "-");
    printf((mode & S_IWUSR) ? "w" : "-");
    if (mode & S_ISUID)
        printf((mode & S_IXUSR) ? "s" : "S");
    else
        printf((mode & S_IXUSR) ? "x" : "-");
    printf((mode & S_IRGRP) ? "r" : "-");
    printf((mode & S_IWGRP) ? "w" : "-");
    if (mode & S_ISGID)
        printf((mode & S_IXUSR) ? "s" : "S");
    else
        printf((mode & S_IXGRP) ? "x" : "-");
    printf((mode & S_IROTH) ? "r" : "-");
    printf((mode & S_IWOTH) ? "w" : "-");
    if (mode & S_ISVTX)
        printf((mode & S_IXUSR) ? "t" : "T");
    else
        printf((mode & S_IXOTH) ? "x" : "-");
}
void print_entry_stat(struct stat *entry_stat)
{
    print_mode(entry_stat->st_mode);
    printf(" %-8lu", entry_stat->st_nlink);
    if (passwd *pwd = getpwuid(entry_stat->st_uid))
        printf(" %-8.8s", pwd->pw_name);
    else
        printf(" %-8d", entry_stat->st_uid);
    if (group *grp = getgrgid(entry_stat->st_gid))
        printf(" %-8.8s", grp->gr_name);
    else
        printf(" %-8d", entry_stat->st_gid);
    printf(" %-8lu", entry_stat->st_size);
    struct tm *tm = localtime(&entry_stat->st_mtime);

    char datestring[256];
    strftime(datestring, sizeof(datestring), nl_langinfo(D_T_FMT), tm);
    printf(" %s", datestring);

}
void print_dir(char const *path, std::size_t depth)
{
    DIR *dp;
    if ((dp = opendir(path)) == nullptr)
    {
        printf("Error: open dir \"%s\" failed.\n", path);
        return;
    }
    chdir(path);
    indented_print("", depth);
    printf("%s:\n", path);
    while (dirent *entry = readdir(dp))
    {
        struct stat entry_stat{};
        lstat(entry->d_name, &entry_stat);
        using namespace std::literals;
        if (entry->d_name == "."sv || entry->d_name == ".."sv)
            continue;
        indented_print("", depth);
        print_entry_stat(&entry_stat);
        printf(" %s\n", entry->d_name);
        if (S_ISDIR(entry_stat.st_mode))
        {
            print_dir(entry->d_name, depth + 4);
        }
    }
    chdir("..");
    closedir(dp);
}
int main(int argc, char *argv[])
{
    if (argc == 1)
        print_dir(".", 0);
    else
        print_dir(argv[1], 0);
}

